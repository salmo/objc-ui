//
//  ViewController.m
//  TimeLinePoc
//
//  Created by SalmoJunior on 12/01/17.
//  Copyright © 2017 CI&T. All rights reserved.
//

#import "TimeLineViewController.h"
#import "TimeLineHeaderDelegate.h"
#import "TimeLineHeaderViewController.h"
#import "TimeLineBodyDataSource.h"
#import "TimeLineBodyTableViewController.h"
#import "StatementModel.h"
#import "TimeLineHeaderDataSource.h"
#import "CardModel.h"

CGFloat const kHighConstraintPriority = 750;
CGFloat const kLowConstraintPriority = 250;

@interface TimeLineViewController () <TimeLineHeaderDelegate, TimeLineHeaderDataSource, TimeLineBodyDataSource>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerBottomConstraint;

@end

@implementation TimeLineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.headerHeightConstraint.priority = kHighConstraintPriority;
    self.headerBottomConstraint.priority = kLowConstraintPriority;
    
    [self.view layoutIfNeeded];
}

#pragma mark - segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"headerSegue"]){
        TimeLineHeaderViewController *vc = (TimeLineHeaderViewController *)segue.destinationViewController;
        
        vc.dataSource = self;
        vc.delegate = self;
    } else if ([segue.identifier isEqualToString:@"bodySegue"]) {
        TimeLineBodyTableViewController *bodyViewController = (TimeLineBodyTableViewController *)segue.destinationViewController;
        
        bodyViewController.bodyDataSource = self;
    }
}

#pragma mark - delegate

- (void)didClickExpandButton:(TimeLineHeaderViewController *) timeLineHeader withStatus:(HeaderStatus) headerStatus {
    switch (headerStatus) {
        case closed:
            self.headerHeightConstraint.priority = kLowConstraintPriority;
            self.headerBottomConstraint.priority = kHighConstraintPriority;
            break;
        case opened:
            self.headerBottomConstraint.priority = kLowConstraintPriority;
            self.headerHeightConstraint.priority = kHighConstraintPriority;
            break;
    }
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        NSLog(@"fim");
    }];
}

#pragma mark - Body DataSource

- (NSArray *)statementsForTimelineBody:(TimeLineBodyTableViewController *)timeLineBody {
    StatementModel *statement1 = [[StatementModel alloc] init];
    statement1.value = 10;
    statement1.statementDescription = @"statement 1";
    
    StatementModel *statement2 = [[StatementModel alloc] init];
    statement2.value = 20;
    statement2.statementDescription = @"statement 2";

    return @[statement1, statement2];
}

#pragma mark - Header DataSource

- (CardModel *)cardForTimelineHeader:(TimeLineHeaderViewController *)timeLineHeader {
    
    CardModel *card = [[CardModel alloc] init];
    
    card.name = @"MasterCard";
    card.number = @"1234 5123 1241 0987";
    card.holder = @"José das Couves";

    return card;
}

@end
