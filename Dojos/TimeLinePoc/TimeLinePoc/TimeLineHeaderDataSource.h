//
//  TimeLineHeaderDataSource.h
//  TimeLinePoc
//
//  Created by SalmoJunior on 12/01/17.
//  Copyright © 2017 CI&T. All rights reserved.
//

@class TimeLineHeaderViewController;
@class CardModel;

@protocol TimeLineHeaderDataSource <NSObject>

- (CardModel *)cardForTimelineHeader:(TimeLineHeaderViewController *)timeLineHeader;

@end
