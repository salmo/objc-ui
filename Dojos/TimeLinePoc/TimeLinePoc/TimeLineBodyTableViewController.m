//
//  TimeLineBodyTableViewController.m
//  TimeLinePoc
//
//  Created by SalmoJunior on 12/01/17.
//  Copyright © 2017 CI&T. All rights reserved.
//

#import "TimeLineBodyTableViewController.h"
#import "TimeLineBodyDataSource.h"
#import "StatementModel.h"

@interface TimeLineBodyTableViewController ()

@property (nonatomic, strong) NSArray *statements;

@end

@implementation TimeLineBodyTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //retorno do DataSource
    if([self.bodyDataSource respondsToSelector:@selector(statementsForTimelineBody:)]) {
        self.statements = [self.bodyDataSource statementsForTimelineBody:self];
        [self.tableView  reloadData];
    }
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.statements.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"statementCell" forIndexPath:indexPath];
    
    StatementModel *statement = self.statements[indexPath.row];
    cell.textLabel.text = statement.statementDescription;
    return cell;
}



@end
