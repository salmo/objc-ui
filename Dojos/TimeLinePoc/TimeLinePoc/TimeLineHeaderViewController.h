//
//  TimeLineHeaderViewController.h
//  TimeLinePoc
//
//  Created by SalmoJunior on 12/01/17.
//  Copyright © 2017 CI&T. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimeLineHeaderDelegate.h"
#import "TimeLineHeaderDataSource.h"

@interface TimeLineHeaderViewController : UIViewController

@property(nonatomic, weak) id <TimeLineHeaderDelegate> delegate;
@property(nonatomic, weak) id <TimeLineHeaderDataSource> dataSource;


@end
