//
//  TimeLineBodyDataSource.h
//  TimeLinePoc
//
//  Created by SalmoJunior on 12/01/17.
//  Copyright © 2017 CI&T. All rights reserved.
//
@class TimeLineBodyTableViewController;

@protocol TimeLineBodyDataSource <NSObject>

- (NSArray *)statementsForTimelineBody:(TimeLineBodyTableViewController *)timeLineBody;

@end
