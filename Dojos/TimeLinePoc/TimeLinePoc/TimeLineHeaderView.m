//
//  TimeLineHeaderView.m
//  TimeLinePoc
//
//  Created by SalmoJunior on 12/01/17.
//  Copyright © 2017 CI&T. All rights reserved.
//

#import "TimeLineHeaderView.h"
#import "CardModel.h"

@interface TimeLineHeaderView()

@property (weak, nonatomic) IBOutlet UILabel *cardName;
@property (weak, nonatomic) IBOutlet UILabel *cardNumber;
@property (weak, nonatomic) IBOutlet UILabel *holderName;

@end

@implementation TimeLineHeaderView

- (void)fillViewWithModel:(CardModel *)card {
    self.cardName.text = card.name;
    self.cardNumber.text = card.number;
    self.holderName.text = card.holder;
}


@end
