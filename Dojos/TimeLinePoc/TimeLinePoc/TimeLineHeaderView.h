//
//  TimeLineHeaderView.h
//  TimeLinePoc
//
//  Created by SalmoJunior on 12/01/17.
//  Copyright © 2017 CI&T. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CardModel;

@interface TimeLineHeaderView : UIView

- (void)fillViewWithModel:(CardModel *)card;

@end
