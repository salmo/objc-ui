//
//  TimeLineBodyTableViewController.h
//  TimeLinePoc
//
//  Created by SalmoJunior on 12/01/17.
//  Copyright © 2017 CI&T. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimeLineBodyDataSource.h"

@interface TimeLineBodyTableViewController : UITableViewController

@property(nonatomic, weak) id <TimeLineBodyDataSource> bodyDataSource;

@end
