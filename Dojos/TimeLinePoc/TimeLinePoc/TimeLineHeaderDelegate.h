//
//  TimeLineHeaderDelegate.h
//  TimeLinePoc
//
//  Created by SalmoJunior on 12/01/17.
//  Copyright © 2017 CI&T. All rights reserved.
//
@class TimeLineHeaderViewController;

typedef enum : NSUInteger {
    opened,
    closed,
} HeaderStatus;

@protocol TimeLineHeaderDelegate <NSObject>

- (void)didClickExpandButton:(TimeLineHeaderViewController *) timeLineHeader withStatus:(HeaderStatus) headerStatus;

@end
