//
//  StatementModel.h
//  TimeLinePoc
//
//  Created by SalmoJunior on 12/01/17.
//  Copyright © 2017 CI&T. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StatementModel : NSObject

@property (nonatomic, strong) NSString *statementDescription;
@property (nonatomic, assign) float value;

@end
