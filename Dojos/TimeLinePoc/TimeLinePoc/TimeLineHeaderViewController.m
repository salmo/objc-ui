//
//  TimeLineHeaderViewController.m
//  TimeLinePoc
//
//  Created by SalmoJunior on 12/01/17.
//  Copyright © 2017 CI&T. All rights reserved.
//

#import "TimeLineHeaderViewController.h"
#import "TimeLineHeaderView.h"

@interface TimeLineHeaderViewController ()

@property (nonatomic, weak) TimeLineHeaderView *mainView;
@property (nonatomic, weak) IBOutlet UIButton *expandCollapseButton;

@end

@implementation TimeLineHeaderViewController

#pragma mark - Getters/Setter

- (TimeLineHeaderView *)mainView {
    return (TimeLineHeaderView *)self.view;
}

#pragma mark - Overrides

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if([self.dataSource  respondsToSelector:@selector(cardForTimelineHeader:)]) {
        
        CardModel *card = [self.dataSource cardForTimelineHeader:self];
        [self.mainView fillViewWithModel:card];
    }
}

#pragma mark - private methods


#pragma mark - actions
- (IBAction)expandCollapseAction:(id)sender {
    if([self.delegate respondsToSelector:@selector(didClickExpandButton:withStatus:)]){
        HeaderStatus status = closed;
        
        if (self.expandCollapseButton.selected) {
            status = opened;
        }
        
        [self.delegate didClickExpandButton:self withStatus:status];
    }

    self.expandCollapseButton.selected = !self.expandCollapseButton.selected;
}

@end
