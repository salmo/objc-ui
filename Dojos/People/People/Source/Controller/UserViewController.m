//
//  ViewController.m
//  People
//
//  Created by SalmoJunior on 06/01/17.
//  Copyright © 2017 CI&T. All rights reserved.
//

#import "UserViewController.h"
#import "ProfileButton.h"
#import "UserView.h"

@interface UserViewController () <ProfileButtonDelegate, UITableViewDelegate>

@property(nonatomic, strong) UserView *mainView;

@end

@implementation UserViewController

#pragma mark - Overrides

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.mainView setProfileDelegate:self];
}

#pragma mark - Getters/setter

- (UserView *)mainView {
    return (UserView *)self.view;
}

#pragma mark - Actions

- (IBAction)searchUser:(UIButton *)sender {
    [self.mainView updateUser:[self.mainView getUser]];
}

#pragma mark - delegates

-(void)didLoadProfileImage:(ProfileButton *)profileButton {
    NSLog(@"Finish loading remote image.");
}

@end
