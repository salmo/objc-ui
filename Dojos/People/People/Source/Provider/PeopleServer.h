//
//  PeopleServer.h
//  test
//
//  Created by SalmoJunior on 05/01/17.
//  Copyright © 2017 CI&T. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^RequestOperationBlock) (NSDictionary *responseObject, NSError *error);

@interface PeopleServer : NSObject

+ (void)getPersonWithLogin:(NSString *)login withCompletion:(RequestOperationBlock)completion;
+ (NSURLRequest *)requestForUserImage:(NSString *)login;

@end
