//
//  Server.m
//  test
//
//  Created by SalmoJunior on 05/01/17.
//  Copyright © 2017 CI&T. All rights reserved.
//

#import "PeopleServer.h"

@implementation PeopleServer

+ (void)getPersonWithLogin:(NSString *)login withCompletion:(RequestOperationBlock)completion {
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSString *urlString = [NSString stringWithFormat:@"https://people.cit.com.br/profile/%@?format=json", login];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSURLRequest *request = [PeopleServer authenticatedRequest:url];
    
    NSURLSessionTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
       
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(result, error);
        });
    }];
    
    [task resume];
}

+ (NSURLRequest *)requestForUserImage:(NSString *)login {
    NSString *urlString = [NSString stringWithFormat:@"https://people.cit.com.br/photos/%@.jpg", login];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSURLRequest *request = [PeopleServer authenticatedRequest:url];
    
    return request;
}

#pragma mark - Private methods

+ (NSURLRequest *)authenticatedRequest:(NSURL *)url {
#warning - Change hardcoded user and password here.
    NSData *nsdata = [@"user:password" dataUsingEncoding:NSUTF8StringEncoding];
    
    // Get NSString from NSData object in Base64
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    NSString *value = [NSString stringWithFormat:@"Basic %@", base64Encoded];
    [request addValue:value forHTTPHeaderField:@"Authorization"];
    
    return request;
}

@end
