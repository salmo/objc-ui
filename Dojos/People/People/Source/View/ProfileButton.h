//
//  ProfileButton.h
//  People
//
//  Created by SalmoJunior on 10/01/17.
//  Copyright © 2017 CI&T. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ProfileButton;

@protocol ProfileButtonDelegate <NSObject>

- (void)didLoadProfileImage:(ProfileButton *)profileButton;

@end

@interface ProfileButton : UIButton

@property (nonatomic, copy) NSString *login;

@property (nonatomic, weak) id <ProfileButtonDelegate> delegate;

@end
