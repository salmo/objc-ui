//
//  ProfileButton.m
//  People
//
//  Created by SalmoJunior on 10/01/17.
//  Copyright © 2017 CI&T. All rights reserved.
//

#import "ProfileButton.h"
#import "UIButton+AFNetworking.h"
#import "PeopleServer.h"

IB_DESIGNABLE
@interface ProfileButton()

@property (nonatomic, weak) IBInspectable UIImage *placeHolderImage;

@end

@implementation ProfileButton

#pragma mark - Getters/setters
- (void)setLogin:(NSString *)login {
    _login = login;
    
    NSURLRequest *imageRequest = [PeopleServer requestForUserImage:login];
    
    __weak typeof (self) weakSelf = self;
    
    [self setBackgroundImageForState:UIControlStateNormal withURLRequest:imageRequest placeholderImage:nil success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
        
        [weakSelf setBackgroundImage:image forState:UIControlStateNormal];
        
        if ([weakSelf.delegate respondsToSelector:@selector(didLoadProfileImage:)]){
            [weakSelf.delegate didLoadProfileImage:weakSelf];
        }
    } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
    }];
}

- (void)setPlaceHolderImage:(UIImage *)placeHolderImage {
    _placeHolderImage = placeHolderImage;
    
    [self setBackgroundImage:placeHolderImage forState:UIControlStateNormal];
}

- (instancetype)initWithFrame:(CGRect)frame withLogin:(NSString *)login {
    self = [super initWithFrame:frame];
    
    if(self){
        [self buttonApperance];
    }
    
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self buttonApperance];
}

- (void)buttonApperance {
    self.layer.cornerRadius = self.frame.size.height / 2;
    self.layer.masksToBounds = YES;
    self.clipsToBounds = YES;
    
    [self layoutIfNeeded];
}

@end
