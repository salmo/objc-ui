//
//  UserView.h
//  People
//
//  Created by SalmoJunior on 10/01/17.
//  Copyright © 2017 CI&T. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileButton.h"

@interface UserView : UIView

-(void)updateUser:(NSString *)user;
-(NSString *)getUser;
-(void)setProfileDelegate:(id<ProfileButtonDelegate>) delegate;

@end
