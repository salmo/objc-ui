//
//  UserView.m
//  People
//
//  Created by SalmoJunior on 10/01/17.
//  Copyright © 2017 CI&T. All rights reserved.
//

#import "UserView.h"
#import "ProfileButton.h"

@interface UserView()

@property (weak, nonatomic) IBOutlet UITextField *loginTextField;
@property (weak, nonatomic) IBOutlet ProfileButton *profileButton;

@end

@implementation UserView

-(void)updateUser:(NSString *)user{
    [self.profileButton setLogin:user];
}

-(NSString *)getUser{
    return self.loginTextField.text;
}

-(void)setProfileDelegate:(id<ProfileButtonDelegate>) delegate{
    self.profileButton.delegate = delegate;
}

@end
