# Instruments

---

# Ferramenta para análise de aplicacões iOS, macOS, watchOS e tvOS. 

![right 100%](https://i1.wp.com/www.tidev.io/wp-content/uploads/2014/01/Apple_Instruments_Icon.png)

---

# Fluxo de trabalho

![inline](https://raw.githubusercontent.com/CIT-SWAT/iOS-Guidelines/master/screenshots/instruments_0.png)

---

# Possui muitas formas de análisar.

![inline](https://raw.githubusercontent.com/CIT-SWAT/iOS-Guidelines/master/screenshots/instruments_1.png)

---

# Sendo os itens abaixo os mais recomendados para aplicações feita em Objective-C:

- Allocations
- Leaks
- Time Profile
- Zombies

---

# Allocations

---

#Allocations

Auxilia para identificar referências de memória abandonadas e quantidade de memória utilizada.
 
O Instruments não identifica a causa, apenas mostra ponteiros que podem não mais ser necessários.

---

# Demo

---

# Leaks

---

# Leaks

É um complemento do Allocations. Com a finalidade de localizar o uso de memória utilizada para alocar objetos que não são mais referenciados ou localizáveis.

---

# Demo

---

# Time Profile

---

# Time Profile

Tem como objetivo mapear o uso do core e threads. Com essa ferramenta é possível identificar a carga em determinada thread além de identificar facilmente qual função está sobrecarregando a CPU.

---

# Demo

---

# Zombies

---

# Zombies

Zombies auxilia na identificação de chamadas para objetos depois de serem desalocados.

---

# Zombies

Para identificar zombies em nosso código, nós precisamos fazer uma configuração no nosso projeto antes de abrir o Instruments.

![inline](https://raw.githubusercontent.com/CIT-SWAT/iOS-Guidelines/master/screenshots/instruments_4.png) ![inline](https://raw.githubusercontent.com/CIT-SWAT/iOS-Guidelines/master/screenshots/instruments_5.png)

---

# Dica

Use o Xcode Gauges durante o desenvolvimento.

![inline](https://i.stack.imgur.com/hU79r.png)

---

# Referências

**Instruments User Guide:** https://developer.apple.com/library/content/documentation/DeveloperTools/Conceptual/InstrumentsUserGuide/