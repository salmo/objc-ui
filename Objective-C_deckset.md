# Objective-C

---

- Property Values
- Callbacks
- iOS Lifecycle
- Separando View da Controller
- Evitando abuso de Singletons
- @class vs. #import

---

## Property Values

```objectivec
@property (???) NSString *someString;
```

Instruções passadas na definição de properties, que ajudam a definir seu comportamento, como acesso, mutabilidade, ownership e multithreading. 

---

```objectivec
getter=methodname
setter=methodname:
nonatomic
atomic
readonly
readwrite
copy
strong
assign
weak
unsafe_unretained
copy
retain
```

---

# **API Control**

```objectivec
getter=methodname
setter=methodname:
```

---

Ao definir uma *property* de forma simplificada

```objectivec
@interface Puppy: NSObject
@property NSString *puppyName;
@end
```

---

O que o compilador na verdade entende é como se você tivesse declarado dois métodos.

```objectivec
@interface Puppy: NSObject
- (NSString *)puppyName;
- (void)setPuppyName: (NSString *)newPuppyName;
@end
```

---

Se desejar, pode usar os atributos como no exemplo abaixo para melhorar a leitura do código.

```objectivec
@property (getter=isHousebroken, setter=setHousebrokenness:) BOOL housebroken;
```

---

Deixando assim de existe os `setter` e `getter` padrões criados pelo compilador.

```objectivec
//getter
if ([puppy isHousebroken]) ... do stuff

//setter
[puppy setHousebrokenness: YES];
```

---

Vale lembrar tbm que você pode usar *dot notation*, mas não existe nenhuma otimização fazendo desta forma, o compilador vai continuar chamando o setter default.

```objectivec
puppy.housebroken = YES;
```

---

# **Read / Write Serialization** (not general thread safety)

```objectivec    
nonatomic
atomic (default)
```

Todo desenvolvedor Objective-C usa `nonatomic` o tempo todo, mas vale reforçar o porque fazemos isso.

---

O padrão `atomic` significa que a *property* é *thread safe*, ou seja, ela pode ser acessada de diferentes threads sem corromper seu valor, as leituras e escritas são serializadas.

---

No exemplo abaixo, temos uma *property* `atomic`.

```objectivec
@property (atomic) CGRect domain;
```

Ela é representada por 4 valores CGFloat
 
![inline 150%](https://www.bignerdranch.com/assets/img/blog/2013/10/puppyrect-1.png)

---

Agora vamos dizer que foram feitos acessos de escrita de diferentes threads.

```
<b>thread 1:</b> puppy.domain = CGRectMake (1.0, 2.0, 3.0, 4.0);
<b>thread 2:</b> puppy.domain = CGRectMake (10.0, 20.0, 30.0, 40.0);
```

O resultado final será:

![inline 150%](https://www.bignerdranch.com/assets/img/blog/2013/10/puppyrect-4.png)

---

E não embaralhada como no caso abaixo, se a *property* fosse definida como `nonatomic` e sofresse vários acessos simultâneos.

```
<b>thread 1:</b> puppy.domain = CGRectMake (1.0, 2.0, 3.0, 4.0);
<b>thread 2:</b> puppy.domain = CGRectMake (10.0, 20.0, 30.0, 40.0);
```

![inline 150%](https://www.bignerdranch.com/assets/img/blog/2013/10/puppyrect-2.png)

---

Mas em uma visão mais realista, em views com muitos abritutos, `atomic` não deixa o seu código *Thread Safe*, e sim garante que o valor de uma determinada propriedade séra sempre completamente alterado, não tendo mix de valores antigos e novos.

---

No exemplo abaixo, os valores das propriedades de um mesmo objeto podem ser alteradas por threads diferentes e cada propriedade receber um valor seguro porém de threads diferentes. 

```objectivec
puppy.name = @"Hoover";
puppy.domain = CGRectMake (1.0, 2.0, 3.0, 4.0);
puppy.housebroken = NO;
```

---

Caso opte por ter uma *property* `atomic` e também sobrescrever um dos métodos de acesso, você deverá prover os dois, `getter` e `setter`, mantendo assim possível a sincronização.

```objectivec
@property (copy) NSString *address;
...
- (void)setAddress:(NSString *)address { ... }
```

---

Caso contrário o warning abaixo será exibido.

```
Writable atomic property 'address' cannot pair a synthesized getter with a user defined setter
```

---

Nesse caso deverá também criar uma `ivar` ou fazer o *synthesize* para armazenar o valor da propriedade, o compilador deixará de fornecer a variavél de instancia padrão com o _ como prexifo.

```objectivec
@synthesize address = _address;
```

---

Vale lembrar que uma *property* `atomic` é mais onerosa para a CPU e bateria, além de deixar o seu código mais lento. Ela é mais utilizada em programas para Mac, onde o poder de processamento é maior.

---

# **Mutability**

```objectivec
readonly
readwrite (default)
```

Por padrão em Objective-C propriedades são mutaveis, podendo receber valores ou nil. Mas pode haver casos em que se deseja uma *property* somente com permissão de leitura.

---

Nesses casos ao setar uma property como `readyonly` o compilador vai criar somente o método `getter` e não criará uma variavel de instancia para armazenar o valor da propriedade.

```objectivec
@property (nonatomic, copy) NSString *firstName;
@property (nonatomic, copy) NSString *lastName;
@property (nonatomic, readonly) NSString *puppyName;
...
- (NSString *) puppyName {
    return [NSString stringWithFormat: @"%@ %@", self.firstName, self.lastName];
}
```

---

Mas pode haver casos em que você deseja que uma *property* seja `readolny` somente na interface publica.

```objectivec
//.h
@property (nonatomic, readonly) NSString *puppyName;

//.m
@interface Puppy()
@property (nonatomic, readwrite) NSString *puppyName;
@end
```

Deixando assim o código mais seguro.

---

# **Memory Management** (ARC)

```objectivec
copy
strong (default)
weak
unsafe_unretained
assign
```

Objetos com ponteiros são `strong` por padrão, mas há casos que devemos muda-los.

---

# copy

No casos de *properties* `NSString` devemos sempre usar `copy`. Assim sempre que passar um valor para essa *property* um nova alocação de memória será criada.

```objectivec
@property (nonatomic, copy) NSString *title;
```

---

Caso queira que sua *property* recebe uma cópia mutável, basta chamar o métodos `-mutableCopy`.

```objectivec
_title = [title mutableCopy];
```

---

# weak

Existem casos em que devemos usar `weak` para evitar *Retain Cycles*. Nos casos de *delegates* e quando se tem uma refência para uma classe
pai.

```objectivec
@property (nonatomic, weak) id <SomethingDelegate> delegate;
```

---

# assign

Classes que não retem ponteiros, como tipos primitivos, devem ser declarados como `assign`.

```objectivec
@property (nonatomic, assign) NSInteger puppyId
```

---

# unsafe_unretained

E o último caso menos comum é o `unsafe_unretained`, ele é utilizado quando precisamos evitar *Retain Cycles* com refências para objetos que não podem receber `weak`, como `NSFont, NSWindow, etc.`

---

# **Memory Management** (Non-ARC)

```objectivec
assign  (default)
retain
copy
```

Para utilizados em projetos *Non-ARC*.

---

É importante, mesmo para os casos defaults, sempre deixar explicito, ficando assim claro como a *property* deveria ser.

---

# Callbacks

Existem dois jeitos de criamos callbacks em Objective-C e devemos saber quando usar cada um deles. 

---

1. Quando um objeto tem mais de um evento distinto, devemos optar por **delegate**.
2. Para **singleton** devemos usar **block**. Objetos podem ter somente um **delegate**.
3. Quando é necessário chamar por informações adicionais devemos usar **delegate**.
4. Com **delegate** é possível manter o estado de uma informação entre diferentes passos.

---

## Delegate
	- Mais ligado a processo

## Blocks
	- Mais voltado a resultado

---

# Delegate

Declaração:

```objectivec
@protocol SomethingDelegate <NSObject>

@required
- (void)didSomethingRequired;

@optional
- (void)didSomethingOptional;

@end
```

---

Ao definir um delegate lembre-se sempre de usar referência fraca com `weak`, evitando *Retain Cycles*.

```objectivec
@property (nonatomic, weak) id <SomethingDelegate> delegate;
```

---

Ao chamar um método de `delegate` em sua classe é sempre importante verificar se quem está adotando-o fez a implementação do método desejado.

```objectivec
if ([self.delegate respondsToSelector:@selector(didSomethingOptional)]) {
    [self.delegate didSomethingOptional];
}
```

---

# Block

Declaração:

```objectivec
//as a property
@property (nonatomic, copy, nullability) returnType (^blockName)(parameterTypes);

//as a method parameter
- (void)someMethodThatTakesABlock:(returnType (^nullability)(parameterTypes))blockName;

```

---

Blocks também podem gerar *Retain Cycles*, e para evita-los devemos uma referência fraca sempre que a classe que fez a chamada é acessada dentro do retorno da closure.

```objectivec
__weak typeof(self) weakSelf = self;

[self.manager getAllClientsWithCompletionBlock:^(NSArray *result) {
    weakSelf.clients = result;
    [weakSelf.tableView reloadData];
}];
```

---

# iOS Lifecycle

![inline](https://i.stack.imgur.com/g19fw.png)

---

Para os métodos com o prexifo `view` no fluxo acima é necessário chamar o `super` sempre que fizer o `override` do método.

```objectivec
- (void)viewDidLoad {
	[super viewDidLoad];
}
```

---

As atualizações de layout em uma UIView customizada devem ser feitas no método abaixo.

```objectivec
- (void)layoutSubviews {
...
}
```

Nesses casos, sempre que houver uma alteração de comportamento, como por exemplo uma rotação do device, esses métodos serão novamente invocados, permitindo assim fazer os ajustes necessários.

---

Na Controller é possível identiicar esse evento nos métodos abaixo:

```objectivec
- (void)viewWillLayoutSubviews {
    ...
}

- (void)viewDidLayoutSubviews {
    ...
}
```

Nesses casos não é necessário chamar o `super`.

---

Mas esses métodos não devem ser invocados diretamente, eles são feitos através dos métodos:

```objectivec
- (void)setNeedsLayout;

- (void)layoutIfNeeded;
```

---

## - setNeedsLayout

Forçar a chamada do `layoutSubviews` antes do próximo redesenho da tela.

---

## - layoutIfNeeded
Força o chamada do `layoutSubviews` imediatamente.

---

Você nunca deve chama-los dentro do `layoutSubviews`, para não deixar sua app em loop.

Para os métodos `setNeedsLayout` e `layoutIfNeeded` devem ser evitados `override`.

---

## Separando View da Controller

Em iOS somos induzidos a criar nosso código de View dentro da Controller. 

Muito pelo Xcode criar somente o arquivo de *Controller* que já tem referência para o objeto da *View*.

**`UIViewController`**

---

Para fazer essa separação, devemos:

- Criar uma nova classe que herde de UIView
- Trocar tipo da View no *Storyboard* ou *Xib*
- Criar uma *property* do tipo da nova *view* no .m da *Controller*
- Fazer override do `getter` da nova *view* retornando um cast da `UIView` detault.

---

Ficando assim o acesso a sua View pela Controller.

```objectivec
@property (nonatomic, weak) CustomView *customView;

...

- (CustomView *)customView {
    return (CustomView *)self.view;
}
```

---

A partir deste ponto você deverá ligar os `Outlets` somente na *View* e as `Actions` na sua *Controller*.

```objectivec
[self.customView updateLabelX:"New Value"];
```

---

# Evitando abuso de Singletons

Como desenvolvedores iOS estamos acostumados a ver muitos *singletons* no nosso dia a dia, classes como NSApplication e NSFileManager são alguns dos muitos exemplos.

---

Apesar de um código estranho é muito fácil de ser criado, existe no Xcode a muito tempo o *snippet* `dispatch_once` que faz a criação automatica de todo esse código.

```objectivec
+ (instancetype)sharedInstance
{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}
```

---

Por esses motivos ele se torna fácil de usar e abusar em aplicações iOS.

Mas tem o lado perigoso, como:
	- Global State
	- Construtor privado
	- Código difícil de ser compreendido
	- Difícil de debugar
	- Side Effect

---

Mas como podemos evitar?
Muito simples, passando a dependencia.

```objectivec
@interface SPFriendListViewController : UIViewController

- (instancetype)initWithUser:(SPUser *)user;

@end
```

---

# @class vs. #import

@class diz ao compilador que um determinado símbolo é uma classe sem a necessidade de considerar todo o conteúdo da interface de uma classe como o #import.

- Economiza tempo do compilador
- Evita import circular

---

# Arquitetura de Referência

https://accelerategit.cit.com.br/accelerate-squad/guidelines-objc-ios