# Objective-C

## Property Values

```Objective-C
@property (???) NSString *someString;
```

Instruções passadas na definição de properties, que ajudam a definir seu comportamento, como acesso, mutabilidade, ownership e multithreading. 

```Objective-C
getter=methodname
setter=methodname:
nonatomic
atomic
readonly
readwrite
copy
strong
assign
weak
unsafe_unretained
copy
retain
```

É importante, mesmo para os casos defaults, sempre deixar explicito, ficando assim claro como a *property* deveria ser.

Mais detalhes sobre cada opção:

**API Control**

```Objective-C
getter=methodname
setter=methodname:
```

Ao definir uma *property* de forma simplificada

```Objective-C
@interface Puppy: NSObject
@property NSString *puppyName;
@end
```

O que o compilador na verdade entende é como se você tivesse declarado dois métodos.

```Objective-C
@interface Puppy: NSObject
- (NSString *)puppyName;
- (void)setPuppyName: (NSString *)newPuppyName;
@end
```

Ele adiciona automaticamente o prefixo `set` para o setter e deixa o mesmo nome da propriedade para o `getter`.

Se desejar, pode usar os atributos como no exemplo abaixo para melhorar a leitura do código.

```Objective-C
@property (getter=isHousebroken, setter=setHousebrokenness:) BOOL housebroken;
```

Deixando assim de existe os `setter` e `getter` padrões criados pelo compilador.

```Objective-C
//getter
if ([puppy isHousebroken]) ... do stuff

//setter
[puppy setHousebrokenness: YES];
```

Vale lembrar tbm que você pode usar *dot notation*, mas não existe nenhuma otimização fazendo desta forma, o compilador vai continuar chamando o setter default.

```Objective-C
puppy.housebroken = YES;
```

---
**Read / Write Serialization** (not general thread safety)

```Objective-C    
nonatomic
atomic (default)
```

Todo desenvolvedor Objective-C usa `nonatomic` o tempo todo, mas vale reforçar o porque fazemos isso.

O padrão `atomic` significa que a *property* é *thread safe*, ou seja, ela pode ser acessada de diferentes threads sem corromper seu valor, as leituras e escritas são serializadas.

No exemplo abaixo, temos uma *property* `atomic`.

```Objective-C
@property (atomic) CGRect domain;
```
 
 Que é representada por 4 valores CGFloat
 
 ![](https://www.bignerdranch.com/assets/img/blog/2013/10/puppyrect-1.png)

Agora vamos dizer que foram feitos acessos de escrita de diferentes threads.

```
<b>thread 1:</b> puppy.domain = CGRectMake (1.0, 2.0, 3.0, 4.0);
<b>thread 2:</b> puppy.domain = CGRectMake (10.0, 20.0, 30.0, 40.0);
```

O resultado final será:

![](https://www.bignerdranch.com/assets/img/blog/2013/10/puppyrect-4.png)

E não embaralhada como no caso abaixo, se a *property* fosse definida como `nonatomic` e sofresse vários acessos simultâneos.

![](https://www.bignerdranch.com/assets/img/blog/2013/10/puppyrect-2.png)

Mas em uma visão mais realista, em views com muitos abritutos, `atomic` não deixa o seu código Thread Safe, e sim garante que o valor de uma determinada propriedade séra sempre completamente alterado, não tendo mix de valores antigos e novos.

No exemplo abaixo, os valores das propriedades de um mesmo objeto podem ser alteradas por threads diferentes e cada propriedade receber um valor seguro porém de threads diferentes. 

```Objective-C
puppy.name = @"Hoover";
puppy.domain = CGRectMake (1.0, 2.0, 3.0, 4.0);
puppy.housebroken = NO;
```

Caso obte por ter uma *property* `atomic` e também sobrescrever um dos métodos de acesso, você deverá prover os dois, `getter` e `setter`, mantendo assim possível a sincronização.

```Objective-C
@property (copy) NSString *address;
...
- (void)setAddress:(NSString *)address { ... }
```

O warning abaixo será exibido.

```
Writable atomic property 'address' cannot pair a synthesized getter with a user defined setter
```

Nesse caso deverá também criar uma `ivar` ou fazer o *synthesize* para armazenar o valor da propriedade, o compilador deixará de fornecer a variavél de instancia padrão com o _ como prexifo.

```Objective-C
@synthesize address = _address;
```

Vale lembrar que uma *property* `atomic` é mais onerosa para a CPU e bateria, além de deixar o seu código mais lento. Ela é mais utilizada em programas para Mac, onde o poder de processamento é maior.

---
**Mutability**

```Objective-C    
readonly
readwrite (default)
```

Por padrão em Objective-C propriedades são mutaveis, podendo receber valores ou nil. Mas pode haver casos em que se deseja uma *property* somente com permissão de leitura.

Nesses casos ao setar uma property como `readyonly` o compilador vai criar o método `getter` e também não criará uma variavel de instancia para armazenar o valor da propriedade.

```Objective-C
@property (nonatomic, copy) NSString *firstName;
@property (nonatomic, copy) NSString *lastName;
@property (nonatomic, readonly) NSString *puppyName;
...
- (NSString *) puppyName {
    return [NSString stringWithFormat: @"%@ %@", self.firstName, self.lastName];
}
```

Mas pode haver casos em que você deseja que uma *property* seja `readolny` somente na interface publica, nesses casos você precisa declara-lá novamente em uma class extention no arquivo de implementação.

```Objective-C
//.h
@property (nonatomic, readonly) NSString *puppyName;

//.m
@interface Puppy()
@property (nonatomic, readwrite) NSString *puppyName;
@end
```

Nesse caso o compilador criará os métodos `getter` e `setter` como privados e você deixa o seu código mais seguro.

---
**Memory Management** (ARC)

```Objective-C
copy
strong (default)
weak
unsafe_unretained
assign
```

Objetos com ponteiros são `strong` por padrão, mas há casos que devemos muda-los.

No casos de *properties* `NSString` devemos sempre usar `copy`. Assim sempre que passar um valor para essa *property* um nova alocação de memória será criada.

```Objective-C
@property (nonatomic, copy) NSString *title;
```

Neste casa sua *property* receba um valor de outra *property* mutável que vai ter o seu valor alterado posteriomente, sua *property* fica imutável e livre de efeitos colaterais. 

Caso queira que sua *property* recebe uma cópia mutável, basta chamar o métodos `-mutableCopy`.

```Objective-C
_title = [title mutableCopy];
```

Existem casos em que devemos usar `weak` para evitar *Retain Cycles*. Nos casos de *delegates* e quando se tem uma refência para uma classe
pai.

```Objective-C
@property (nonatomic, weak) id <SomethingDelegate> delegate;
```

E o último caso menos comum é o `unsafe_unretained`, ele é utilizado quando precisamos evitar *Retain Cycles* com refências para objetos que não podem receber `weak`, como `NSFont, NSWindow, etc.`

Classes que não retem ponteiros, como tipos primitivos, devem ser declarados como `assign`.

```Objective-C
@property (nonatomic, assign) NSInteger puppyId
```

---
**Memory Management** (Non-ARC)

```Objective-C
assign  (default)
retain
copy
```

Para utilizados em projetos *Non-ARC*.

---
## Callbacks

Exsitem dois jeitos de termos callbacks em Objective-C, e devemos saber quando usar cada um deles. Abaixo algumas considerações:

1. Quando um objeto tem mais de um evento distinto, devemos optar por `delegate`.
2. Para `singleton` devemos usar `block`. Objetos podem ter somente um `delegate`.
3. Quando é necessário chamar por informações adicionais devemos usar `delegate`.
4. Com `delegate` é possível manter o estado de uma informação entre diferentes passos.

- Delegate
	- Mais ligado a processo
- Blocks
	- Mais voltado a resultado

### Delegate

Declaração:

```Objective-C
@protocol SomethingDelegate <NSObject>

@required
- (void)didSomethingRequired;

@optional
- (void)didSomethingOptional;

@end
```

Ao definir um delegate lembre-se sempre de usar referência fraca com `weak`, evitando *Retain Cycles*.

```Objective-C
@property (nonatomic, weak) id <SomethingDelegate> delegate;
```

Ao chamar um método de `delegate` em sua classe é sempre importante verificar se quem está adotando-o fez a implementação do método desejado.

```Objective-C
if ([self.delegate respondsToSelector:@selector(didSomethingOptional)]) {
    [self.delegate didSomethingOptional];
}
```

---
### Block

Declaração:

```Objective-C
//as a property
@property (nonatomic, copy, nullability) returnType (^blockName)(parameterTypes);

//as a method parameter
- (void)someMethodThatTakesABlock:(returnType (^nullability)(parameterTypes))blockName;

```

Blocks também podem gerar *Retain Cycles*, e para evita-los devemos uma referência fraca sempre que a classe que fez a chamada é acessada dentro do retorno da closure.

```Objective-C
__weak typeof(self) weakSelf = self;

[self.manager getAllClientsWithCompletionBlock:^(NSArray *result) {
    weakSelf.clients = result;
    [weakSelf.tableView reloadData];
}];
```

---
## iOS Lifecycle

![](https://i.stack.imgur.com/g19fw.png)

Para os métodos com o prexifo `view` no fluxo acima é necessário chamar o `super` sempre que fizer o `override` do método.

```Objective-C
- (void)viewDidLoad {
	[super viewDidLoad];
}
```

O lugar mais indicado para fazer alguma atualização de layout em uma UIViewController são nos métodos:

```Objective-C
- (void)viewDidLayoutSubviews {
    ...
}

- (void)viewWillLayoutSubviews {
    ...
}
```

Nesses casos não é necessário chamar o `super`.

No caso de você ter uma UIView customizada, você deve fazer suas ajustes no método abaixo:

```Objective-C
- (void)layoutSubviews {
...
}
```

Nesses casos sempre que houver uma alteração de comportamento, como por exemplo uma rotação do device, esses métodos serão novamente invocados, permitindo assim fazer os ajustes necessários.

Mas esses métodos não devem ser invocados diretamente, eles são feitos através dos:

Forçar a chamada do `layoutSubviews` antes do próximo redesenho da na tela.

```Objective-C
- (void)setNeedsLayout;
```

Força o chamada do `layoutSubviews` imediatamente.

```Objective-C
- (void)layoutIfNeeded;
```

Você nunca deve chamar os métodos acima dentro do `layoutSubviews`, para não deixar sua app em loop.

Para os métodos `setNeedsLayout` e `layoutIfNeeded` devem ser evitados `override`.

## Evitando abuso de Singletons

Como desenvolvedores iOS estamos acostumados a ver muitos *singletons* no nosso dia a dia, classes como NSApplication e NSFileManager são alguns dos muitos exemplos.

```Objective-C
+ (instancetype)sharedInstance
{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}
```

Apesar de um código estranho é muito fácil de ser criado, existe no Xcode a muito tempo o *snnipet* `dispatch_once` que faz a criação automatica de todo esse código.

Por esses motivos ele se torna fácil de usar e abusar em aplicações iOS.

Mas tem o lado perigoso, como:
	- Global State
	- Construtor privado
	- Código difícil de ser compreendido
	- Difícil de debugar
	- Side Effect

Mas como podemos evitar?
Muito simples, passando a dependencia.

```Objective-C
@interface SPFriendListViewController : UIViewController

- (instancetype)initWithUser:(SPUser *)user;

@end
```


