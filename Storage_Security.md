# Storage

---

# Storage

- User Defaults
- SQLite
- File System

---

# User Defaults

Armazenamento simples de chave-valor. Usado apenas para salvar configurações simples, não é seguro.

```objectivec
NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults]

//Save data
[defaults setBool:YES forKey:@"Walkthrough"];

//Get data
Bool walkthrough = [defaults boolForKey:@"Walkthrough"];
```

---

# SQLite

Banco de dados nativo em iOS. Wrappers da lib SQLite3 indicados:

- **FMDB (Objc)**: https://accelerategit.cit.com.br/accelerate-squad/database-management-objc-ios

- **SQLite.swift**: https://accelerategit.cit.com.br/accelerate-squad/database-management-swift-ios

---

# File System

- Bundles
- Documents
- Library
- Temp

---

# File System

- Bundles

```objectivec
NSURL* url = [[NSBundle mainBundle] URLForResource:@"MyImage" withExtension:@"png"];
```

- Documents

```objectivec
NSFileManager *fileManager = [NSFileManager defaultManager];
NSArray *directories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
NSString *documentsPath = [directories firstObject];
NSString *filePath = [documentsPath stringByAppendingPathComponent:@"file.txt"];
BOOL fileExists = [fileManager fileExistsAtPath:filePath];
```

---

# Security

---

# Security

- Common Crypto
- SQLCipher
- Keychain
- Certificate Pinning

---

# Common Crypto

Biblioteca nativa que permite a geração de hashes dinâmicos.

```swift
func sha256Hex(string: String) -> String? {
    guard let messageData = string.data(using:String.Encoding.utf8) else { return nil }
    var digestData = Data(count: Int(CC_SHA256_DIGEST_LENGTH))

    _ = digestData.withUnsafeMutableBytes {digestBytes in
        messageData.withUnsafeBytes {messageBytes in
            CC_SHA256(messageBytes, CC_LONG(messageData.count), digestBytes)
        }
    }

    return digestData.map { String(format: "%02hhx", $0) }.joined()
}
```

---

# SQLCipher

Criptografia do banco de dados, supportada por ambos meccanismos recomendados.

- **Objc**: https://accelerategit.cit.com.br/accelerate-squad/secure-database-objc-ios
- **Swift**: https://accelerategit.cit.com.br/accelerate-squad/secure-database-swift-ios

---

# Keychain

Mecanismo seguro para armazenar tokens, key etc.

- **Objc**: https://accelerategit.cit.com.br/accelerate-squad/keychain-objc-ios
- **Swift**: https://accelerategit.cit.com.br/accelerate-squad/keychain-swift-ios

---

# Certificate Pinning

Mecanimos para fazer requisições de serviço de forma segura. Veja exemplos usando NSURLSession, AFNetworking e Alamofire:

- **Objc**: https://accelerategit.cit.com.br/accelerate-squad/certificate-pinning-ios-objc
- **Swift**: https://accelerategit.cit.com.br/accelerate-squad/certificate-pinning-ios-swift